package com.example.spinner;

public class Animal {
    private String especie;
    private int imageAnimal;

    public Animal(String especie, int imageAnimal) {
        this.especie = especie;
        this.imageAnimal = imageAnimal;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public int getImageAnimal() {
        return imageAnimal;
    }

    public void setImageAnimal(int imageAnimal) {
        this.imageAnimal = imageAnimal;
    }
}
