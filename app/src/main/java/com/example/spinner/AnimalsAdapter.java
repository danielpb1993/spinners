package com.example.spinner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class AnimalsAdapter extends ArrayAdapter<Animal> {

    public AnimalsAdapter (Context context, ArrayList<Animal> animalArrayList) {
        super(context, 0, animalArrayList);
    }

    //Ctrl + o -> sobreescriu mètodes
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return init(position, convertView, parent);
    }

    public View init (int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row, parent, false
            );
        }

        //hook

        ImageView imageRow = convertView.findViewById(R.id.imageRow);
        TextView textRow = convertView.findViewById(R.id.textRow);

        //setText i setImageResource
        Animal currentItem = getItem(position);

        if (currentItem != null) {
            imageRow.setImageResource(currentItem.getImageAnimal());
            textRow.setText(currentItem.getEspecie());
        }
        return convertView;
    }
}
