package com.example.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.mtp.MtpConstants;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Spinner spinner;
    private Spinner spinner2;
    private Spinner spinnerClass;

    List<String> entries2 = new ArrayList<>();

    //SpinnerClass
    private ArrayList<Animal> mAnimal;
    private AnimalsAdapter mAnimalsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        spinnerClass = findViewById(R.id.spinnerClass);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.entries)
        );
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_checked);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                String selectedCondition = parent.getItemAtPosition(i).toString();
                Toast.makeText(MainActivity.this, selectedCondition, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();
            }
        });

        entries2.add(0, "Select entry");
        entries2.add("XS");
        entries2.add("S");
        entries2.add("M");
        entries2.add("L");
        entries2.add("XL");
        entries2.add("XXL");
        entries2.add("XXXL");

        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                entries2
        );

        spinner2.setAdapter(adapter2);
        adapter2.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = adapterView.getItemAtPosition(i).toString();
                Toast.makeText(MainActivity.this, selection, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();

            }
        });

        /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.entries)
        );

        adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item
        );
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing", Toast.LENGTH_SHORT).show();
            }
        });*/

        //SpinnerClass
        initList();
        mAnimalsAdapter = new AnimalsAdapter(this, mAnimal);
        spinnerClass.setAdapter(mAnimalsAdapter);

        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Animal selectedItem = (Animal) adapterView.getItemAtPosition(i);
                String selectedEspecie = selectedItem.getEspecie();
                Toast.makeText(MainActivity.this, selectedEspecie, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this, "Nothing to show", Toast.LENGTH_SHORT).show();

            }
        });
    }

    //

    private void initList(){
        mAnimal = new ArrayList<>();
        mAnimal.add(new Animal("Cat", R.drawable.cat ));
        mAnimal.add(new Animal("Duck", R.drawable.duck ));
        mAnimal.add(new Animal("Puppy", R.drawable.puppy ));
    }
}